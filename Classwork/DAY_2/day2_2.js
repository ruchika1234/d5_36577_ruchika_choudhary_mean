// create object using Object

function fun1(){
    const p1 = new Object()
    console.log(p1)
}

// fun1()

function fun2(){
    const p1 = new Object()

    p1.name = "ruchi"
    p1.email = "ruchi@gmail.com"
    p1.age = 23

    console.log(p1)

    const m1 = new Object()

    m1["name"] = "note pro"
    m1["company"] = "oppo"
    m1["price"] = 12000

    console.log(m1)
}

// fun2()

function printDetails(){
    console.log(`name: ${this["name"]}`)
    console.log(`email: ${this["email"]}`)
    console.log(`age: ${this["age"]}`)
}

function fun3(){
    const p1 = new Object()
    p1.name = "payal"
    p1.email = "payal@test.com"
    p1.age = 26
    p1.printDetail = printDetails
    console.log(p1)
    p1.printDetail()

    const p2 = new Object()
    p2.name = "deep"
    p2.email = "deep@test.com"
    p2.age = 23
    p2.printDetail = printDetails
    console.log(p2)
    p2.printDetail()

}

fun3()