//create object using json
function fun1(){
    const person= {
        name: 'person1',
        email: "person@gmail.com",
        age: 32
    }

    console.log(person)
    console.log(`type of person = ${typeof(person)}`)

}

// fun1()

function fun2(){
    const person = [
        {name: "ruchi", email: "ruchi@gmail.com"},
        {name: "neelu", email: "neelu@gmail.com"},  
        {name: "maya", email: "mayu@gmail.com"},  
        {name: "pooja", email: "pooji@gmail.com"}
    ]

    console.log(person)
    console.log(`type of person = ${typeof(person)}`)
}

// fun2()

function canVote(person){
     if(person['age'] >= 18){
         console.log(`person ${person['name']} is eligible for voting`)
     }else {
        console.log(`person ${person['name']} is NOT eligible for voting`)
     }
}

function fun3(){
    const p1 = {name: "ruchi", email: "ruchi@gmail.com", age: 20}
    canVote(p1)

    const p2 = {name: "deep", email: "deep@gmail.com", age: 13}
    canVote(p2)

    const p3 = {name: "payal", email: "payal@gmail.com", age: 19}
    canVote(p3)
}

// fun3()

function fun4(){
    const p1 = {name: "ruchi", email: "ruchi@gmail.com", age: 20}

    console.log(`name: ${p1['name']}`)
    console.log(`email: ${p1['email']}`)
    console.log(`age: ${p1['age']}`)
}

// fun4()

function fun5(){
    const p1 = {name: "ruchi", email: "ruchi@gmail.com", age: 20}

    console.log(`name: ${p1.name}`)
    console.log(`email: ${p1.email}`)
    console.log(`age: ${p1.age}`)
}

// fun5()

function fun6(){
    const person = { 'first name': 'ruchi', 'last name': 'jat' }

    console.log(`first name: ${person["first name"]}`)
    console.log(`last name: ${person["last name"]}`)
}

// fun6()

function canVotePerson(){
    console.log("this data")
    console.log(this)
    if(this['age'] >= 18){
        console.log(`person ${this['name']} is eligible for voting`)
    }else {
       console.log(`person ${this['name']} is NOT eligible for voting`)
    }
}

function fun7(){
    const p1 = {name: "ruchi", email: "ruchi@gmail.com", age: 20}

    p1.canVote = canVotePerson
    console.log(p1)

    //OOP
    // p1.canVote(p1)
    p1.canVote()
}

// fun7()

function fun8(){
    const p1 = {name: "ruchi", email: "ruchi@gmail.com", age: 20, "canVote": canVotePerson}
    const p2 = {name: "deep", email: "deep@gmail.com", age: 13, "canVote": canVotePerson}
    const p3 = {name: "payal", email: "payal@gmail.com", age: 19, "canVote": canVotePerson}

    // p1.canVote = canVotePerson
    // p2.canVote = canVotePerson
    // p3.canVote = canVotePerson

    p1.canVote()
    p2.canVote()
    p3.canVote()
}

// fun8()

function fun9(){
    console.log(arguments)
    console.log(this)
}

fun9()