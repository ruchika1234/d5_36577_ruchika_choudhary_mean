              
function function1() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const squares = numbers.map((number) => {
      return number * number
    })
    console.log(numbers)
    console.log(squares)
  }
  
  //function1()

  function function2() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const squares = numbers.map((number) => { number * number })
    console.log(numbers)
    console.log(squares)
  }