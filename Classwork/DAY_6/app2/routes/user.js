const express = require('express')
const { request, response } = require('express')

const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/', (request, response) => {
    response.send()
})

router.post('/signin', (request, response) => {
    const email = request.body.email
    const password = request.body.password

    const statement = `select * from user where email = '${email}' and password = '${password}';`

    db.query(statement, (error, users) => {

        const result = {status: ''}
        if (users.length == 0){
            //user does not exist
            result['status'] = 'error'
            result['error'] = 'user does not exist'
        } else {
            //user exist
            const user = users[0]
            result['status'] = 'success'
            result['data'] = {
                id: user['id'],
                email: user['email'],
                firstname: user['firstname'],
                lastname: user['lastname']
            }
        }

        response.send(result)

    })
})

router.post('/signup', (request, response) => {
    const firstname = request.body.firstname
    const lastname = request.body.lastname
    const email = request.body.email
    const password = request.body.password
    const phone = request.body.phone

    const statement = `insert into user(firstname, lastname, email, password, phone) values(
        '${firstname}', '${lastname}', '${email}', '${password}', '${phone}')`

    db.query(statement, (error, dbResult) => {
        response.send( utils.createResults(error, dbResult))
    })
})

router.put('/', (request, response) => {
    response.send()
})

router.delete('/', (request, response) => {
    response.send()
})

module.exports = router