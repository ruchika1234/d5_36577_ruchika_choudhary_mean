const express = require('express')
const { request, response } = require('express')

const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/get', (request, response) => {

    const statement = `select * from note;`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResults(error, dbResult))

    })
})

router.post('/insert', (request, response) => {
    const userid = request.body.userid
    const contents = request.body.contents

    const statement = `insert into note (userid, contents) values('${userid}', '${contents}');`

    db.query(statement, (error, dbResult) => {
    
        response.send(utils.createResults(error, dbResult)) 
    })

})

router.put('/', (request, response) => {
    response.send()
})

router.delete('/', (request, response) => {
    response.send()
})

module.exports = router