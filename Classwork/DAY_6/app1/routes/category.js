const express = require('express')
const { request, response } = require('express')

// router is used to handle the route
const router = express.Router()

router.get('/', (request, response) => {
    console.log('insode GET /category')
    response.send('GET /category')
})

router.post('/', (request, response) => {
    console.log('inside POST /category')
    response.send('POST /category')
})

router.put('/', (request, response) => {
    console.log('inside PUT /category')
    response.send('PUT /category')
})


router.delete('/', (request, response) => {
    console.log('inside DELETE /category')
    response.send('DELETE /category')
})
// the exported router can be imported and added into the server
module.exports = router