const express = require('express')
const { request, response } = require('express')

// router is used to handle the route
const router = express.Router()

router.get('/user', (request, response) => {
    console.log('insode GET /user')
    response.send('GET /user')
})

router.post('/signin', (request, response) => {
    console.log('inside POST /user')
    response.send('POST /user/signin')
})

router.post('/signup', (request, response) => {
    console.log('inside POST /user')
    response.send('POST /user/signup')
})
// the exported router can be imported and added into the server
module.exports = router