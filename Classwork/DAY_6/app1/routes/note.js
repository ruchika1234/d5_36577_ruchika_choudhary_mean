const express = require('express')
const { request, response } = require('express')

// router is used to handle the route
const router = express.Router()

router.get('/', (request, response) => {
    console.log('insode GET /note')
    response.end('GET /note')
})

router.post('/', (request, response) => {
    console.log('insode POST /note')
    response.end('POST /note')
})

router.put('/', (request, response) => {
    console.log('insode PUT /note')
    response.end('PUT /note')
})
module.exports = router