// require the express package
const express = require('express')
const { request, response } = require('express')

// create a server instance
const app = express()

function log(request, response, next) {
  console.log('inside log function')
  next()
}

// function add(p1, p2) {
//   return p1 + p2
// }

// const answer = add(10, 20)

function log2() {
  console.log('inside log2')
  return (request, response, next) => {
    console.log('inside the log2 arrow function')
    next()
  }
}

const answer = (request, response, next) => {
  console.log('inside the answer arrow function')
  next()
}

// const answer = log2()

// function alias
app.use(log)
// app.use(answer)
app.use(log2())

app.get('/', (request, response) => {
  console.log('inside GET /')
  response.send('done')
})

// listen on port 3000 (start the server)
app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})
