// require the express package
const express = require('express')
const { response, request } = require('express')

const bodyParser = require('body-parser')

// create a server instance
const app = express()

app.use(bodyParser.json())

//require mysql for db connection
const mysql = require('mysql')

//---------------------
//----------product routes
//------------------------

app.get('/product', (request, response) => {

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `select * from products`

    //execute the statement
    connection.query(statement, (error, data) => {

        // close the connection
        connection.end()

        if (error) {
            response.end(`error: ${error}`)
        } else {

            console.log(data)

            // const string = JSON.stringify(data)

            response.send(data)

            // response.setHeader('Content-Type', 'application/json')

            // // data is array of products
            // // response.end(data)
            // response.end(string)
        }
    })
})

app.post('/product', (request, response) => {
    console.log(`body: `)
    console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `insert into products (name, price)
               values ('${request.body.name}',
                       '${request.body.price}')`

    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

app.put('/product', (request, response) => {
    console.log(`body: `)
    console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `update products set
                       name = '${request.body.name}',
                       price = '${request.body.price}'
                       where id = '${request.body.id}'`                   

    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

app.delete('/product', (request, response) => {
    console.log(`body: `)
    // console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `delete from products where id = ${request.body.id}`
    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

// listen on port 3000 (start the server)
app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
})

// {
//     "title": "new product from postman",
//     "price": 100000,
//     "description": "this is a dummy description",
//     "category": "this is a dummy category",
//     "userId": 1,
//     "company": "this is a dummy company"
//     } 