// require the express package
const express = require('express')
const { request, response } = require('express')

// create a server instance
const app = express()

function log1(request, response, next){
    console.log('inside log1 function')

    console.log(`method: ${request.method}`)
    console.log(`url: ${request.url}`)

    next()
}

function log2(request, response, next){
    console.log('inside log2 function')

    console.log(`method: ${request.method}`)
    console.log(`url: ${request.url}`)

    next()
}

function log3(request, response, next){
    console.log('inside log3 function')

    console.log(`method: ${request.method}`)
    console.log(`url: ${request.url}`)

    next()
}

app.use(log2)
app.use(log3)
app.use(log1)

app.get('/', (request, response) => {
    console.log('inside GET /')
    response.end('this is GET /')
})

// listen on port 3000 (start the server)
app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
  })
  