const express = require('express')
const utils = require('../../utils.js')
const db = require('../../db')
const config = require('../../config')
const router = express.Router()
//-----------------------------------------------------------
// GET

/**
 * @swagger
 *
 * /brand:
 *   get:
 *     description: list of brands
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful message
 */

router.get('/', (request, response) => {
    const statement = `select * from brand`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST

/**
 * @swagger
 *
 * /brand:
 *   post:
 *     description: list of brands
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.post('/', (request, response) => {
    const { title, description} = request.body
    const statement = `insert into brand (title, description) values ('${title}', '${description}')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT


/**
 * @swagger
 *
 * /brand:
 *   put:
 *     description: update the brands
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.put('/:id', (request, response) => {
    const { id } = request.params
    const { title, description} = request.body
    const statement = `update brand set title = '${title}', description = '${description}' where id = '${id}'`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE

/**
 * @swagger
 *
 * /brand/delete:
 *   delete:
 *     description: delete admin data
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful done
 */

router.delete('/:id', (request, response) => {
    const {id } = request.params
    const statement = `delete from brand where id = '${id}'`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})

//-----------------------------------------------------------


module.exports = router