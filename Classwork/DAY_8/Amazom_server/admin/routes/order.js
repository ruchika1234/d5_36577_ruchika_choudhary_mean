const express = require('express')
const utils = require('../../utils.js')
const db = require('../../db')
const config = require('../../config')
const router = express.Router()
//-----------------------------------------------------------
// GET

/**
 * @swagger
 *
 * /order:
 *   get:
 *     description: list of orders
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful message
 */

router.get('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST

/**
 * @swagger
 *
 * /order:
 *   post:
 *     description: order for product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.post('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT


/**
 * @swagger
 *
 * /order:
 *   put:
 *     description: update order data
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.put('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE


/**
 * @swagger
 *
 * /order/delete:
 *   delete:
 *     description: delete order
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful done
 */

router.delete('', (request, response) => {
    response.send()
})

//-----------------------------------------------------------


module.exports = router