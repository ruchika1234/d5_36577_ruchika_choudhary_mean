const express = require('express')
const utils = require('../../utils.js')
const db = require('../../db')
const config = require('../../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const { secret } = require('../../config')

const router = express.Router()
//-----------------------------------------------------------
// GET
//-----------------------------------------------------------

/**
 * @swagger
 *
 * /admin/profile:
 *   get:
 *     description: profile of the user
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: get all data successfully
 */


router.get('/profile', (request, response) => {
    const statement = `select firstName, lastName, phone, email from admin where id = '${request.userId}'`
    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'admin not found' })
            } else {
                const admin = admins[0]
                response.send(utils.createResult(error, users))
            }
        }
    })

})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST
//-----------------------------------------------------------

/**
 * @swagger
 *
 * /admin/signup:
 *   post:
 *     description: signup to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: firstname to use for signup.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: lastname is used for signup
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: email is used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: password is used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */

router.post('/signup', (request, response) => {
    const { firstName, lastName, email, password } = request.body

    const encryptedPassword = crypto.SHA256(password)
    const statment = `insert into admin (firstName, lastName, email, password) values (
      '${firstName}', '${lastName}', '${email}', '${encryptedPassword}'
  )`

    db.query(statment, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

/**
 * @swagger
 *
 * /admin/signin:
 *   post:
 *     description: signin to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email is used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: password is used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */


router.post('/signin', (request, response) => {
    const { email, password } = request.body

    const encryptedPassword = crypto.SHA256(password)
    const statement = `select id, firstName, lastName from admin where email = '${email}' and password = '${encryptedPassword}'`

    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'user not found' })
            } else {
                const admin = admins[0]
                const token = jwt.sign({ id: admin['id'] }, config.secret)
                response.send(utils.createResult(error, {
                    firstName: admin['firstName'],
                    lastName: admin['lastName'],
                    token: token

                }))
            }
        }
    })
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT
//-----------------------------------------------------------

/**
 * @swagger
 *
 * /admin/update:
 *   put:
 *     description: update the admin profile
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: firstname to use for signup.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: lastname is used for signup
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: email is used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: password is used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */

router.put('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE
//-----------------------------------------------------------

/**
 * @swagger
 *
 * /admin/delete:
 *   delete:
 *     description: delete admin data
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful message
 */

router.delete('', (request, response) => {
    response.send()
})

//-----------------------------------------------------------


module.exports = router