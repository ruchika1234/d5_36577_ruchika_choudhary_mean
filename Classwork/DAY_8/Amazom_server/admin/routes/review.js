const express = require('express')
const utils = require('../../utils.js')
const db = require('../../db')
const config = require('../../config')
const router = express.Router()
//-----------------------------------------------------------
// GET

/**
 * @swagger
 *
 * /review:
 *   get:
 *     description: review of product
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful message
 */

router.get('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST

/**
 * @swagger
 *
 * /review:
 *   post:
 *     description: review of product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.post('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT


/**
 * @swagger
 *
 * /review:
 *   put:
 *     description: review of product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.put('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE


/**
 * @swagger
 *
 * /review/delete:
 *   delete:
 *     description: delete review of product
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful done
 */

router.delete('', (request, response) => {
    response.send()
})

//-----------------------------------------------------------


module.exports = router