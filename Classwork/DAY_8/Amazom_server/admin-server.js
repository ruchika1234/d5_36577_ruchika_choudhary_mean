const express = require('express')
const bodyParser = require('body-parser')
const { request, response } = require('express')
const config = require('./config')
const jwt = require('jsonwebtoken')

//morgan: for logging:
const morgan = require('morgan')

// swagger: for api documentation
const swaggerJSdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')



//routers
const adminRouter = require('./admin/routes/admin')
const brandRouter = require('./admin/routes/brand')
const categoryRouter = require('./admin/routes/category')
const orderRouter = require('./admin/routes/order')
const productRouter = require('./admin/routes/product')
const reviewRouter = require('./admin/routes/review')


const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))

// swagger initiaization
const swaggerOptions = {
    definition: {
        info: {
            title: 'Amazon Server',
            version: '1.0.0',
            description: 'this is a express server for amazon application'
        }
    },

    apis: ['./admin/routes/*.js']
}

const swaggerSpec = swaggerJSdoc(swaggerOptions)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// add a middleware for getting the id from token
function getUserId(request, response, next) {

    if(request.url == '/admin/signup'  || request.url == '/admin/signin'){
        next()
    } else {

    try {
        const token = request.headers['token']
        const data = jwt.verify(token, config.secret)

        // add a new key named userId with logged in user's id
        request.userId = data['id']

        //gomto the actual route
        next()


    } catch(ex){
        response.status(401)
        response.send({status: 'error', error: 'protected api'})

    }

}
    
}

app.use(getUserId)


// add the routes
app.use('/admin', adminRouter)
app.use('/brand', brandRouter)
app.use('/category', categoryRouter)
app.use('/order', orderRouter)
app.use('/product', productRouter)
app.use('/review', reviewRouter)




// default route
app.get('/', (request, response) => {
    response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})