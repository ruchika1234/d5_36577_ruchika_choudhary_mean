import { RegisterService } from './../register.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  firstname = ''
  lastname = ''
  address = ''
  contact = 0
  email = ''
  password = ''

  constructor(
    private registerService: RegisterService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onRegister(){
    this.registerService.register(this.firstname, this.lastname, this.address, this.contact, this.email, this.password)
    .subscribe(response => {
      if(response['status'] == 'success'){
        console.log(response['data']);
        alert("register successfully")
        
      } else{
        alert("error")
        console.log(response['errro']);
        
      }
    })
  }

}
