import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class LoginService{

  url = 'http://localhost:3000/admin'
  constructor( 
    private router :Router,
    private httpClient: HttpClient ) { }

  login(email: string, password: string){

    const body = {
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/login', body)

  }  

  // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  //   if(sessionStorage['token']){
  //     return true
  //   }

  //   this.router.navigate(['/'])

  //   return false
  // }
}
