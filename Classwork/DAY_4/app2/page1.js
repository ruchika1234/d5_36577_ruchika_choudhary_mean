const mysql = require('mysql')

function getProducts() {

    //step 1.1: create connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb',
        port: 3306
    })

    //step 1.1: open the connectio
    // connection.connect()

    //step 2: prepare SQL query
    const statement = `select * from products`;

    // step 3: execute the query
    //asynchronous
    connection.query(statement, (error, data) => {
        console.log(`error: ${error}`)
        // console.log(data)

        // step 4: process the result
        //console.log(data)
        // for(let index = 0; index < data.length; index++){
        //     const row = data[index];

        //     console.log(`id => ${row['id']}`)
        //     console.log(`name => ${row['name']}`)
        //     console.log(`price => ${row['price']}`)
        //     console.log("--------------------------------")
        // }

        console.table(data)

        //step 5: close the connection
        connection.end()
    })
}

getProducts()