//step 1: reqire express
const express = require('express')
const { request, response } = require('express')

// step 2: create express application
const app = express()

// step3: Route (mapping of http GET, url and handler (function))
// GET /
// app.get('/', (request, response) => {
//     console.log('GET / received')
//     response.end('FROM GET /')
// })

app.get('/product', (request, response) => {
    console.log('select * from product')
    response.end('FROM GET /')
})

app.post('/product', (request, response) => {
    console.log('insert product...')
    response.end('FROM POST /')
})

app.put('/product', (request, response) => {
    console.log('update product...')
    response.end('FROM PUT /')
})

app.delete('/product', (request, response) => {
    console.log('delete product...')
    response.end('FROM DELETE /')
})

//step 4: listen on port 3000
app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})