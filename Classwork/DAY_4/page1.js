// step 1: get the http module
const http = require('http')

// step 2: create a http server
const server = http.createServer((request, response) => {
    console.log("a request recieved")

    console.log(`url: ${request.url}`)
    console.log(`method: ${request.method}`)

    // read the response as a string
    // response.setHeader('Content-Type', 'text/html')
    // response.setHeader('Content-Type', 'text/plain')

    // read the response as json
    response.setHeader('Content-Type', 'application/json')

    //send the response
    // response.end("<h4>hello from server<h4>")
    // response.end('{"name": "ruchi", "company": "PG-DAC"}')
    response.end('[{"name": "Ruchi", "company": "PG-DAC"}, {"name": "Payal", "company": "PG-DMC"}]')
})

// step 3: start the server
server.listen(3000, '0.0.0.0', () => {
    console.log("server started successfully")
})