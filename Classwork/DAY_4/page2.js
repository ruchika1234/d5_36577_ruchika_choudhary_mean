const http = require('http')

const server = http.createServer((request, response) => {
    console.log("request recieved")

    if(request.url == '/product'){
        if(request.method == 'GET'){
            console.log('select * from product')
        } else if(request.method == 'POST'){
            console.log('insert into product')
        } else if(request.method == 'PUT'){
        console.log('update product ....')
        } else if(request.method == 'DELETE'){
            console.log('delete product ....')
        }
    }else if(request.url == '/category'){
        if(request.method == 'GET'){
            console.log('select * from category')
        } else if(request.method == 'POST'){
            console.log('insert into category')
        } else if(request.method == 'PUT'){
        console.log('update category ....')
        } else if(request.method == 'DELETE'){
            console.log('delete category ....')
        }
    } else if(request.url == '/moblie'){
        if(request.method == 'GET'){
            console.log('select * from mobile')
        } else if(request.method == 'POST'){
            console.log('insert into mobile')
        } else if(request.method == 'PUT'){
        console.log('update mobile ....')
        } else if(request.method == 'DELETE'){
            console.log('delete mobile ....')
        }
    }

    response.end("request processed")

})

server.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})