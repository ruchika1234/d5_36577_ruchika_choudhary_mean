const nodemailer = require('nodemailer')
const { request } = require('express')
const config = require('./config')
function sendEmail(email, subject, body, callback) {
    const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.emailUser,
            pass: config.password
        }
    })

    const options = {
        from: config.emailUser,
        to: email,
        subject: subject,
        html: body
    }

    transport.sendMail(options, callback)

    // transport.sendMail(options, (error, info) => {
    //     console.log(error)
    //     console.log(info)
    // })
}

module.exports = {
    sendEmail: sendEmail
}

// sendEmail(
//     'jatd3445@gmail.com',
//     `<h1>Welocme to my store</h1>

//     <div>HELLO,</div>

//     <div>WELCOME TO MY STORE</div>
//     <div>here..................</div>

//     <div> Best regards,</div>
//     <div>Admin</div>`
// )