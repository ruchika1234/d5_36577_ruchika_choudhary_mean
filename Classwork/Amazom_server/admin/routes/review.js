const express = require('express')
const utils = require('../../utils.js')
const db = require('../../db')
const config = require('../../config')
const router = express.Router()
//-----------------------------------------------------------
// GET

/**
 * @swagger
 *
 * /review:
 *   get:
 *     description: review of product
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful message
 */

router.get('/:productId', (request, response) => {
   const {productId} = request.params

    const statement = `select id, userId, review, rating from productReviews where productId = '${productId}' `
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST

/**
 * @swagger
 *
 * /review:
 *   post:
 *     description: review of product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

// router.post('/:productId', (request, response) => {
//     const { productId } = request.params
//     const { review, rating} = request.body

//     const statement = `insert into productReview (review, productId, userId, rating) values (
//         '${review}', '${productId}', '${request.userId}', '${rating}'
//     )`
//     db.query(statement, (error, data) => {
//         response.send(utils.createResult(error, data))
//       })

// })
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT


/**
 * @swagger
 *
 * /review:
 *   put:
 *     description: review of product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.put('', (request, response) => {
    response.send()
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE


/**
 * @swagger
 *
 * /review/delete:
 *   delete:
 *     description: delete review of product
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful done
 */

router.delete('/:id', (request, response) => {
    const {id} = request.params

    const statement = `delete from productReviews where id = '${id}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})

//-----------------------------------------------------------


module.exports = router