const express = require('express')
const utils = require('../../utils.js')
const db = require('../../db')
const config = require('../../config')

//multer for uploading document
const multer = require('multer')
const { request, response } = require('express')
const upload = multer({dest: 'images/'})

const router = express.Router()

//-----------------------------------------------------------
// GET

/**
 * @swagger
 *
 * /product:
 *   get:
 *     description: list of products
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful message
 */

router.get('/', (request, response) => {
    // const statement = `select p.id, p.title, p.description, p.price, p.brand, p.image, p.image, c.id, c.title, c.description  from product p inner join category c on p.category = c.id inner join brand b on p.brand = b.id `
    const statement = `
    select p.id, p.title, p.description, 
    c.id as categoryId, c.title as categoryTitle,
    b.id as brandId, b.title as brandTitle,
    p.price, p.image, p.isAvtive from product p
  inner join category c on c.id = p.category
  inner join brand b on b.id = p.brand
    `
    db.query(statement, (error, data) => {
      if(error){
          response.send(utils.createError(error))
      } else {
          //empty products collections
          const products = []

          // iterate over the collection and modify the structure
          for(let index = 0; index < data.length; index++){
              const tmpProduct = data[index];
              const product = {
                  id: tmpProduct['id'],
                  title: tmpProduct['title'],
                  description: tmpProduct['description'],
                  price: tmpProduct['price'],
                  brand: {
                      id: tmpProduct['brandId'],
                      title: tmpProduct['brandTitle']
                  },
                  image: tmpProduct['image'],
                  isAvtive: tmpProduct['isAvtive'],
                  category: {
                      id: tmpProduct['categoryId'],
                      title: tmpProduct['categoryTitle']
                  }

              }

              products.push(product)

          }
          response.send(utils.createSuccess(products))

      }
    })
})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST



 router.post('/upload-image/:id', upload.single('image'), (request, response) => {
    const { id } = request.params
    const fileName = request.file.filename

    const statement = `update product set image = '${fileName}' where id = '${id}'`
    // console.log(request.file)
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
 })

 /**
 * @swagger
 *
 * /product:
 *   post:
 *     description: insert data into product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: category
 *         description: category of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: price
 *         description: price of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: brand
 *         description: brand of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.post('/create', (request, response) => {
    const { title, description, category, price, brand} = request.body
    const statement = `insert into product (title, description, category, price, brand) values (
        '${title}', '${description}', '${category}', '${price}', '${brand}'
      )`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT


/**
 * @swagger
 *
 * /product:
 *   put:
 *     description: update data into product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: category
 *         description: category of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: price
 *         description: price of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: brand
 *         description: brand of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful done
 */

router.put('/:id', (request, response) => {
    const { id } = request.params
    const {title, description, category, price, brand} = request.body
    const statement = `update product set
           title = '${title}',
           description = '${description}',
           category = '${category}',
           price = '${price}',
           brand = '${brand}'
           where id = '${id}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })       
})

router.put('/update-status/:id/:isActive', (request, response) => {
    const {id, isActive} = request.params

    const statement = `update product set
    isAvtive = '${isActive}'
    where id = '${id}'`

db.query(statement, (error, data) => {
 response.send(utils.createResult(error, data))
})
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE


/**
 * @swagger
 *
 * /product/delete:
 *   delete:
 *     description: delete product
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful done
 */

router.delete('/:id', (request, response) => {
    const id = request.params
    const statement = `delete from product where id = '${id}'`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//-----------------------------------------------------------


module.exports = router