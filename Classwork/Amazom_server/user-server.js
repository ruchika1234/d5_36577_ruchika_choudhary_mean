const express = require('express')
const bodyParser = require('body-parser')
const { request, response } = require('express')
const config = require('./config')
const jwt = require('jsonwebtoken')

//morgan: for logging:
const morgan = require('morgan')

// swagger: for api documentation
const swaggerJSdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

//routes
const userRouter = require('./user/routes/user')
const orderRouter = require('./user/routes/order')

const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))

// swagger initiaization
const swaggerOptions = {
    definition: {
        info: {
            title: 'Amazon Server (user front)',
            version: '1.0.0',
            description: 'this is a express server for amazon application'
        }
    },

    apis: ['./user/routes/*.js']
}

const swaggerSpec = swaggerJSdoc(swaggerOptions)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// add a middleware for getting the id from token
function getUserId(request, response, next) {

    if(request.url == '/user/signup'
       || request.url == '/user/signin'
       || request.url.startsWith('/user/activate') 
       || request.url == '/logo.png'
       || request.url.startsWith('/user/forgot-password')){
        next()
    } else {

    try {
        const token = request.headers['token']
        const data = jwt.verify(token, config.secret)

        // add a new key named userId with logged in user's id
        request.userId = data['id']

        //gomto the actual route
        next()


    } catch(ex){
        response.status(401)
        response.send({status: 'error', error: 'protected api'})

    }

}
    
}

app.use(getUserId)

//requires to send static images in the directory named image    
app.use(express.static('images/'))


// add the routes
app.use('/user', userRouter)
app.use('/order', orderRouter)



// default route
app.get('/', (request, response) => {
    response.send('welcome to my application')
})

app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
})