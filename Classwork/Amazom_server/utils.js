function createResult(error, data) {
   return error ? createError(error) : createSuccess(data);
}

function createError(error){
    const result = {}
        result['status'] = 'error'
        result['error'] = error
    // else {
    //     result['status'] = 'success'
    //     result['data'] = data
    // }

    return result

}

function createSuccess(data) {
    const result = {}
    result['status'] = 'success'
    result['data'] = data
  
    return result
  }

  function generateOTP() {
      const min = 10000
      const max = 99999
      return Math.floor(Math.random() * (max - min) + min)
  }
  

module.exports = {
    createResult: createResult,
    createSuccess: createSuccess,
    createError: createError,
    generateOTP: generateOTP
}