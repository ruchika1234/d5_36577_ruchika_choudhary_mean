import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  productService: ProductService

  products = []

  constructor( productService: ProductService) { 
    this.productService = productService
  }

  ngOnInit(): void {
    this.loadProduct()
  }

  loadProduct(){
      const request = this.productService.getProduct()
      request.subscribe(response => {
        if(response['status'] == 'success'){
          this.products = response['data']
          console.log(response['data'])
        } else {
          console.log(response['error'])
          alert("error while loading products ") 
        }
      })
  }

  onDelete(product){
    this.productService.deleteProduct(product['id'])
    .subscribe(response => {
      if(response['status'] == 'success'){
        this.loadProduct()
      } else {
        console.log(response['error'])
      }
    })
  }

}
