import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = 'http://localhost:3000/product'

  httpClient: HttpClient

  // productService class is dependent on HttpClient
  // - dependency injection
  //     -Angular will inject (pass) an object of HttpClient
  //      while creating an object of ProjectService class  
  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient
   }

   getProduct(){
     const request = this.httpClient.get(this.url)
     return request
    }

   createProduct(title: string, price: number, description: string, category: number, brand: number){
     const body = {
       title: title,
       price: price,
       description: description,
       category: category,
       brand: brand
     }

     return this.httpClient.post(this.url, body)
     
   }

   editProduct(){

   }

   deleteProduct(id: number){
     return this.httpClient.delete(this.url + "/" + id)

   }


}


// class person {
//   name: string

//   constructor(name: string) {
//     this.name = name
//   }
// }

// const p1 = new person('person1')