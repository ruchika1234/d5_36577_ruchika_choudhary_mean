// abstract class Run {
//     public abstract run()
// }

interface Run {
    run()
}

class Animal implements Run {
    run() {
        console.log("animal is running")
    }
}

// contract between service provider and service consumer
interface IDrawable {
    draw()
    erase()
}

// service provider
class Rectangle implements IDrawable {
    draw() {
        console.log('drawing rectangle')
    }
    erase() {
        console.log('erasing rectangle')
    }

    rectangle() {
        console.log("rectangle fun")
    }
  
}

class Circle implements IDrawable {
    draw() {
        console.log('drawing Circle')
    }
    erase() {
        console.log('erasing Circle')
    }
  
}

class Square implements IDrawable {
    draw() {
        console.log('drawing Square')
    }
    erase() {
        console.log('erasing Square')
    } 
}

// type of reference = type of object
const drawable1: IDrawable = new Rectangle()
drawable1.draw()
drawable1.erase()

const drawable2: IDrawable = new Circle()
drawable2.draw()
drawable2.erase()

const drawable3: IDrawable = new Square()
drawable3.draw()
drawable3.erase()
