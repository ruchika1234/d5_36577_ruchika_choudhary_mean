// base class
// super class
class Person {
    protected name: string = ""
    protected address: string = ''
    protected age: number = 0

    public constructor (name: string, address: string, age: number){
        this.name = name
        this.address = address
        this.age = age
    }
}

// derived class
// sub class
class Player extends Person {
    team: string = ""

    public constructor (name: string, address: string, age:number, team:string){
        // cslling super class(person) constructor
        super(name, address, age)
        this.team = team
    }

    public printInfo() {
        console.log(`name = ${this.name}`)
        console.log(`age = ${this.age}`)
        console.log(`address = ${this.address}`)
        console.log(`team = ${this.team}`)

    }
}

const person1 = new Person("perosn1", "pune", 20)
console.log(person1)

const player1 = new Player('player1', 'mumbai', 10, 'india')
console.log(player1)
player1.printInfo()
