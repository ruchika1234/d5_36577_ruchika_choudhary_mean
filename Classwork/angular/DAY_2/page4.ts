class Person {
    private _name: string
    private _age: number

    // public constructor() {
    //     console.log("inside constructor")
    //     this._name = ""
    //     this._age = 0
    // }

    // constructor
    public constructor(name: string = '', age: number = 0) {
           this._name = name
           this._age = age
    }

    // setter
    public set name(name: string){ this._name = name }
    public set age(age: number){ this._age = age }

    // getter
    public get name() { return this._name}
    public get age() { return this._age}

    // facilitator or utility
    public canVote() {
        if (this._age >= 18) { console.log(`${this._name} is eligible for voting`)}
        else { console.log(`${this._name} is not eligible for voting`)}
    }
    }
}

const p1 = new Person("person1", 20)
// p1.name = "ruchi"
// p1.age = 20
console.log(`name = ${p1.name}`)
console.log(`age = ${p1.age}`)
p1.canVote()

const p2 = new Person()
p2.name = "perosn2"
p2.age = 10

console.log(`name = ${p2.name}`)
console.log(`age = ${p2.age}`)
p2.canVote()