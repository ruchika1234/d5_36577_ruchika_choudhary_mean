abstract class Person {
    name: string
    age: number

    constructor(name: string, age: number) {
        this.name = name
        this.age = age
    }

    printInfo() {
        console.log(`name: ${this.name}`)
        console.log(`age: ${this.age}`)

    }
    
    // incomplete
    // subclass MUST implement this method
    public abstract printProfession() 
}

class Player extends Person {
    team: string

    constructor(name: string, age: number, team: string) {
        super(name, age)
        this.team = team
        console.log(`team: ${this.team}`)
    }

    printProfession() {
        console.log("professsion: player")
    }

}

class Developer extends Person {
    company: string

    constructor(name: string, age: number, company: string) {
        super(name, age)
        this.company = company
    }

    printProfession() {
        console.log("profession: development")
    }

}

// can not create object of abstract class
// const person1 = new Person("person1", 34)
// person1.printInfo()

const player1 = new Player("player1", 25, "india")
player1.printInfo()
player1.printProfession()

const d1 = new Developer("dev 1", 23, "comapny1")
d1.printInfo()
d1.printProfession()