class Mobile {
    // by default all data members are public
    // public model: string
    // public company: string
    // public price: number

    private model: string
    private company: string
    private price: number

    // getter
    public getModel(): string {
        return this.model
    }

    public getCompany(): string {
        return this.company
    }

    public getPrice(): number {
        return this.price
    }

    // setter
    public setModel(model: string) {
        this.model = model
    }

    public setCompany(company: string) {
        this.company = company
    }

   
  public setPrice(price: number) {
    this.price = price
  }
}

// const m1 = new Mobile()
// m1.model = "i-phone"
// m1.company = "apple"
// m1.price = 14400
// console.log(m1)

const m2 = new Mobile()
m2.setModel("m30s")
m2.setCompany("samsung")
m2.setPrice(12000)
console.log(m2)

console.log(m2.getPrice)
console.log(`model = ${m2.getModel()}`)