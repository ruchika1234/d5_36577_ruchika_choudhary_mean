class Person {
    // implicit property declaration
    name

    // explicit property declaration
    age: number
    email: string
    address: string
}

const p1 = new Person()
p1.name = "ruchi"
p1.age = 23
p1.address = "pune"
p1.email = "ruchi@gmail.com"
console.log(p1)
