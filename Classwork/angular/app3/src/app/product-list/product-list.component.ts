import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = [
    {id: 1, title: "prodjuct1", description: "this is a nice product 1", category: "electronices", price: 100},
    {id: 2, title: "prodjuct2", description: "this is a nice product 2", category: "home decor", price: 200},
    {id: 3, title: "prodjuct3", description: "this is a nice product 3", category: "computer", price: 300}
  ]

  constructor() { }

  ngOnInit(): void {
  }

  onEdit(product){
    alert(`editing this product ${product['title']}`)

  }

  onDelete(product, index){
    const result = confirm(`Are you sure you want to delete product ${product['title']}`)
    if(result){
      this.products.splice(index, 1)
    } 
  }

}
