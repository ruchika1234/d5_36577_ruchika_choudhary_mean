import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  title = ''
  description = ''
  price = ''
  category = ''

  constructor() { }

  ngOnInit(): void {
  }

  onTitleChange(event){
    console.log('inside title change')
    console.log(event)
  }

  onKeyDown(event){
     console.log("  on key down")
    console.log(event)
  }

  onKeyUpTitle(event){
    console.log("on key up")
    // console.log(event)
    this.title = event.target.value
  }

  onKeyUpPrice(event){
    this.price = event.target.value
  }

  onKeyUpDescription(event){
    this.description = event.target.value
  }

  onSave(event){
    // console.log(event)
    console.log(`title = ${this.title}`)
    console.log(`price = ${this.price}`)
    console.log(`description = ${this.description}`)

  }

  onCancel(event){
    // console.log(event)
  }

}
