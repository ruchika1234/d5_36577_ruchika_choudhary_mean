import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  title = ''
  description = ''
  price = 0

  product = {}

  constructor(
    private router: Router,
    private activateRoute:ActivatedRoute,
    private ProductService: ProductService) { }

  ngOnInit(): void {
   const id = this.activateRoute.snapshot.queryParams['id']
   if(id){
     this.ProductService.getProductsDetails(id)
     .subscribe(response => {
       if(response['status'] == 'success'){
         const products = response['data']
         if(products.length > 0){
          this.product = products[0]
          this.title = this.product['title']
          this.price = this.product['price']
          this.description = this.product['description']
          
         }
       }
     })
   }
  }

  onUpdate(){
   this.ProductService.updateProductDetails(this.product['id'], this.title, this.description, this.price)
   .subscribe(response => {
     if(response['status'] == 'success'){
         this.router.navigate(['/product-list'])
     } else{
       alert("error")
       console.log(response['error']);
       
     }
   })
  }

}
