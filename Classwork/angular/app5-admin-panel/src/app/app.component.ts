import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router){}

  onLogout(){
    sessionStorage.removeItem['firstName']
    sessionStorage.removeItem['lastName']
    sessionStorage.removeItem['token']

    console.log("hellp")
    this.router.navigate(['/login'])
  }
}
