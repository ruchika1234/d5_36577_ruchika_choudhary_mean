import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = 'http://localhost:3000/product'

  constructor(private httpClient: HttpClient ) { }

  getProducts(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }

    return this.httpClient.get(this.url, httpOptions)
  }

  updateProductDetails(id, title: string, description: string, price: number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }

    const body = {
      title: title,
      description: description,
      price: price
    }

    return this.httpClient.put(this.url + "/" + id, body,  httpOptions)
  }

  getProductsDetails(id){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }

    return this.httpClient.get(this.url + "/details/" + id,  httpOptions)
  }

  toggleActiveStatus(product){

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }

    const body = {
      status: product['isAvtive'] == 1 ? 0 : 1
    }
    return this.httpClient.put(this.url + '/toggle-active/' + product['id'], body, httpOptions)
  }
  
}
