import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = []

  constructor(
    private router: Router,
    private productService: ProductService) { }

  ngOnInit(): void {
    this.loadProducts()
  }


  loadProducts(){
    console.log('inside')
    this.productService.getProducts()
    .subscribe(response => {
      if(response['status'] == 'success'){
        this.products = response['data']
        console.log(response['data'])
      } else{
        console.log(response['error'])
      }
    })
  }

  toggleActive(product){
    this.productService.toggleActiveStatus(product)
.subscribe(response => {
      if(response['status'] == 'success'){
        this.loadProducts()
      } else{
        console.log(response['error'])
      }
    })  }

  onEdit(product){
    this.router.navigate(['/product-add'], {queryParams: {id: product['id']}})

  }  

}
