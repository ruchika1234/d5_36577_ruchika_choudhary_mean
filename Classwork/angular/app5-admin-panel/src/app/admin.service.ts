import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminService implements CanActivate {

  url = 'http://localhost:3000/admin'

  constructor(
    private router: Router,
    private httpClient: HttpClient) { }
  

  login(email: string, password: string){
    const body = {
      email: email,
      password: password
    }
    console.log(this.url);
    console.log(`${email}, ${password}`)
    
    // console.log(body);
    return this.httpClient.post(this.url + '/signin' ,body)
// return  this.httpClient.post('http://localhost:3000/admin/signin ',body)

  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if(sessionStorage['token']){
    // user is alredy logged in
    //launch the component
    return true
  }

  alert("login first")

  //force user to login
  this.router.navigate(['/login'])
    // user has not logged in yet
    //stop launching the component
    return false

  }  
}
