import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit {

  // 1:grren, 2:red, 3: blue
  boxType = 1

  category = 1

  constructor() { }

  ngOnInit(): void {
  }

}
