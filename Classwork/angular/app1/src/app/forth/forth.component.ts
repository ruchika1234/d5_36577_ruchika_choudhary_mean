import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forth',
  templateUrl: './forth.component.html',
  styleUrls: ['./forth.component.css']
})
export class ForthComponent implements OnInit {

  //1: red, 2: green. 3: brown, 4: blue, 5: yellow 
  boxColor = 2

  constructor() { }

  ngOnInit(): void {
  }

  // changeRedColor(){ this.boxColor = 1 }
  // changeGreenColor(){ this.boxColor = 2 }
  // changeBrownColor(){ this.boxColor = 3 }
  // changeBlueColor(){ this.boxColor = 4 }
  // changeYellowColor(){ this.boxColor = 5   }

  changeColor(color) { this.boxColor = color }

  handleClick() {
    alert("button clicked")
  }
}
