import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  firstName = "ruchika"
  lastName = "choudhary"

  person = {
    name: "person1",
    age: 30,
    address: "ratlam",
    phone: 123456,
    email: "ruchi@gmail.com"
  }

  constructor() { }

  ngOnInit(): void {
  }

}
