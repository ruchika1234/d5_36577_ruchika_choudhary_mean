import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = ''
  password= ''

  httpClient: HttpClient

  constructor(
    httpClient: HttpClient ) {
      this.httpClient = httpClient
     }

  ngOnInit(): void {
  }

  onLogin(){
    console.log(`email = ${this.email}`)
    console.log(`password = ${this.password}`)

    const body = {
      email: this.email,
      password: this.password
    }

    const url = 'http://localhost:3000/admin/signin'
    const request = this.httpClient.post(url, body)
    request.subscribe(response => {
      console.log(response)
      if(response['status'] == 'success'){
        const data = response['data']
        const full_name = data['full_name']
        alert(`welcome ${full_name}`)

      } else{
        alert("invalid email or password")
      }
    })
  }

  onCancel(){

  }
}
