class Person {
    private _name: string
    private _age: number
    private _address: string

    public set name(name: string) { this._name = name }
    public set age(age: number) { this._age = age }
    public set address(address: string) { this._address = address }

    public get name() { return  this._name }
    public get age() { return  this._age }
    public get address() { return  this._address }

}