import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mobile-list',
  templateUrl: './mobile-list.component.html',
  styleUrls: ['./mobile-list.component.css']
})
export class MobileListComponent implements OnInit {

  mobiles = [
    {
      id: 1,
      model: "iphone XS maz",
      company: "Apple",
      price: 14400
    },
    {
      id: 2,
      model: "iphone 7 plus",
      company: "Apple",
      price: 98000
    },
    {
      id: 3,
      model: "iphone 6 plus",
      company: "Apple",
      price: 9600
    },
    {
      id: 4,
      model: "iphone 4",
      company: "Apple",
      price: 60000
    },
    {
      id: 5,
      model: "iphone 3G",
      company: "Apple",
      price: 45000
    },
    {
      id: 6,
      model: "Z10",
      company: "BlackBerry",
      price: 40000
    },
    {
      id: 7,
      model: "Galaxy S3",
      company: "Samsung",
      price: 43000
    },
    {
      id: 8,
      model: "Nexus one",
      company: "HTC",
      price: 3000
    },
    {
      id: 9,
      model: "Nokia 770",
      company: "Nokia",
      price: 41000
    },
    {
      id: 10,
      model: "Note 5 pro",
      company: "Xiomi",
      price: 20000
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

  Edit(mobile){
    alert(`edit mobile ${mobile['model']}`)
  }


  Delete(mobile, index){
    const answer = confirm(`are you sure you want to delete ${mobile['model']}` )
    if(answer){
      alert("deleting......")
      this.mobiles.splice(index, 1)
    }
  }
}
