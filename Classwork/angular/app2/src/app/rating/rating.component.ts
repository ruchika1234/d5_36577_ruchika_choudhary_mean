import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  rating = 4
  ratings = [0, 1, 2, 3, 4]

  constructor() { }

  ngOnInit(): void {
  }

}
