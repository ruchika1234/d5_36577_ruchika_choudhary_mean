const fs = require('fs')

function synchronousReadFile() {
    //reading the file
    console.log("file reading started")

    try{
    //blocking call/ API
    const data = fs.readFileSync('./day3.txt')
    console.log("file reading finished")
    console.log(`data = ${data}`)
    console.log("bye bye")
    } catch(ex){

        console.log(`exception: ${ex}`)
    }

    //mathematical calculation
    console.log("performing multiplication")
    const result = 23 * 23
    console.log(`result = ${result}`)

}

// synchronousReadFile()

function asynchronousReadFile(params) {

    console.log("reading file started")

    // starts a thread to perform the read operation
    fs.readFile('./day3.txt', (error, data) => {
        
        //the reading is finished
        console.log("file rading finished")
        
        if(error){
            console.log(`error: ${error}`)
        } else {
            console.log(`data: ${data}`)
        }
        console.log("bye bye")
    })

    //perform mathematical calculation
    console.log("performing multiplication")
    const result = 23 * 23
    console.log(`result = ${result}`)
}

asynchronousReadFile()

function fun1() {

    console.log("download started")
    setTimeout(() => {
        console.log("download finished")
    }, 5000)
    
    //perform mathematical calculation
    console.log("performing multiplication")
    const result = 23 * 23
    console.log(`result = ${result}`)

    console.log("performing another task")
    setTimeout(() => {
        console.log("another task finished")
    }, 10000)
}

// fun1()

function myReadFile(path, func) {

    //func = (error, data) => {..}
    const data = fs.readFileSync(path)
    setTimeout(() => {
        func(null, data)
    }, 10000)
    
}

function fun2() {

    console.log("reading started")
    myReadFile('./day3.txt', (error, data) => {
        console.log(`data = ${data}`)
        console.log("reading finished")
    })
    
        //perform mathematical calculation
        console.log("performing multiplication")
        const result = 23 * 23
        console.log(`result = ${result}`)
}

// fun2()