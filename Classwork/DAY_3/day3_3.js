const persons = [
    { name: 'person1', age: 40 },
    { name: 'person2', age: 10 },
    { name: 'person3', age: 20 },
    { name: 'person4', age: 50 },
  ]
  
  function myfunction() {
    console.log('inside myfunction')
  }
  
  const num = 100
  
  let salary = 14.56

  module.exports = {
      person: persons,
      function: myfunction,
      num: num,
      salary: salary
  }