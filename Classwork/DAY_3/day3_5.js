// importing a system (node) module
const os = require('os')

console.log(`architecture: ${os.arch()}`)
console.log(`platform: ${os.platform()}`)
console.log(`Host name: ${os.hostname()}`)
console.log(`home dir: ${os.homedir()}`)
console.log(`total memory: ${os.totalmem()}`)
console.log(`free memory: ${os.freemem()}`)
console.log(`cpus`)
console.log(os.cpus())
console.log(`network`)
console.log(os.networkInterfaces())



