
const num = 30
console.log(`num = ${num}, type of num = ${typeof(num)}`)

const salary = 9.8
console.log(`salary = ${salary}, type of salary = ${typeof(salary)}`)

const firstName = 'ruchi'
console.log(`firstName = ${firstName}, type of firstName = ${typeof(firstName)}`)

const canVote = true
console.log(`canVote = ${canVote}, type of canVote = ${typeof(canVote)}`)

let myVar
console.log(`myVar = ${myVar}, type of myVar = ${typeof(myVar)}`)

const person = { "name": "person1", age: 50 }
console.log(`person = ${person}, type of person = ${typeof(person)}`)
