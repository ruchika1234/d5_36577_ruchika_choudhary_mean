function function1() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    const isEven = numbers.filter((number) => {
             return number % 2 == 0
    })

    const isOdd = numbers.filter((number) => {
        return number % 2 != 0
    })

    const sqaures = isEven.map((number) => {
        return number * number
    })

    const cube = isOdd.map((number) => {
        return number * number * number
    })

    console.log(isEven)
    console.log(isOdd)
    console.log(sqaures)
    console.log(cube)

}

// function1()

function function2() {
    const cars = [
        { model: "i20", price: 7.5, company: "hyundai", color: "space gray" },
        { model: "nano", price: 2.5, company: "tata", color: "yellow" },
        { model: "x5", price: 35.5, company: "BMW", color: "dark blue" }
    ]

    // get model and company of affordable cars
    const affordableCars = cars.filter((car) => {
        return car['price'] < 10
    })
    const newAffordableCars = affordableCars.map((getCar) => {
        return {model: getCar['model'], company: getCar['company']}
    })

    // get price of non-affordable cars
    const nonAffordableCars = cars.filter((car) => {
        return car['price'] > 10
    })
    const newNonAffordableCars = nonAffordableCars.map((getPrice) => {
        return {price: getPrice['price']}
    })

    console.log(newAffordableCars)
    console.log(newNonAffordableCars)
}

// function2()


function function3() {
    const persons = [
        { name: "person1", email: "person1@test.com", age: 14 },
        { name: "person2", email: "person2@test.com", age: 45 },
        { name: "person3", email: "person3@test.com", age: 50 },
        { name: "person4", email: "person4@test.com", age: 16 }
    ]

    // get name and age of persons who can vote
    const canVote = persons.filter((person) => {
        return person['age'] > 17
    })
    const newPersonCanVote = canVote.map((person) => {
        return {name: person['name'], age: person['age'] }
    })

    // get name and email of persons who can not vote
    const canNotVote = persons.filter((person) => {
        return person['age'] < 18
    })
    const newPersonCanNotVote = canNotVote.map((person) => {
        return {name: person['name'], email: person['email'] }
    })

    console.log(newPersonCanVote)
    console.log(newPersonCanNotVote)

}

function3()
