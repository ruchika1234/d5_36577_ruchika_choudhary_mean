// implicit
function add(p1, p2){
    console.log(`addition = ${p1 + p2}`)
}

// add(10, 20)
// add(10, '20')

// explicit
function subtract(p1: number, p2: number) {
    console.log(`substraction = ${p1 - p2}`)
}

// subtract(20, 3)

// not okk
// subtract("test", "test2")

//int square(int num) { return num * num }

function square(num: number) : number {
   return num * num
}

const number = square(2)
console.log(`sqaure of num = ${number}`)
