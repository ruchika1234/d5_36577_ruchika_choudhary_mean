let num = 100
console.log(`num = ${num}, type = ${typeof(num)}`)

num = "test"
console.log(`num = ${num}, type = ${typeof(num)}`)

num = true
console.log(`num = ${num}, type = ${typeof(num)}`)

 num = {name: "test", age:23}
console.log(`num = ${num}, type = ${typeof(num)}`)

num = 200
console.log(`num = ${num}, type = ${typeof(num)}`)
