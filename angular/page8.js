// variables
// let firstName = "person1"
// age = 30
//function
// function printfInfo() {
// }
var Person = /** @class */ (function () {
    function Person() {
    }
    // method
    Person.prototype.printInfo = function () {
        console.log("first name = " + this.firstName);
        console.log("age = " + this.age);
        console.log("address = " + this.address);
    };
    return Person;
}());
var p1 = new Person();
console.log(p1);
p1.firstName = "ruchi";
p1.age = 23;
p1.address = "ratlam";
// console.log(p1)
p1.printInfo();
// function  Persom(firstName, age, address) {
//     this.firstName = firstName
//     this.age = age
//     this.address = address
// }
// const p1 = new Person("ruchi", 22, "Ratlam")
