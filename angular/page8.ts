// variables
// let firstName = "person1"
// age = 30

//function
// function printfInfo() {
    
// }

class Person {
    // data member of the class (c++)
    // property of the class (Ts)
    // fields of the class (java)
    firstName: string
    age: number
    address: string

    // method
    printInfo(){
        console.log(`first name = ${this.firstName}`)
        console.log(`age = ${this.age}`)
        console.log(`address = ${this.address}`)

    }
}

const p1 = new Person()
console.log(p1)
p1.firstName = "ruchi"
p1.age = 23
p1.address = "ratlam"
// console.log(p1)
p1.printInfo()

// function  Persom(firstName, age, address) {
//     this.firstName = firstName
//     this.age = age
//     this.address = address
    
// }

// const p1 = new Person("ruchi", 22, "Ratlam")