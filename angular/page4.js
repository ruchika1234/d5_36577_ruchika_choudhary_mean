// int num = 100;
var num;
// console.log(`num = ${num}, type = ${typeof(num)}`)
// ok
num = 100;
console.log("num = " + num + ", type = " + typeof (num));
// not okk
// num = "str"
//string
var firstName = "ruchi";
console.log("firstName = " + firstName + ", type = " + typeof (firstName));
//string
var lastName = 'ruchi';
console.log("lastName = " + lastName + ", type = " + typeof (lastName));
//string
var address = "\naddress,\ncity,\nstate\n";
console.log("address = " + address + ", type = " + typeof (address));
//boolean
var canVote = true;
console.log("canVote = " + canVote + ", type = " + typeof (canVote));
//string
var myvar;
console.log("myvar = " + myvar + ", type = " + typeof (myvar));
// myvar = 100
// object
var person = { name: "person1", age: 40, address: "pune" };
console.log("person = " + person + ", type = " + typeof (person));
