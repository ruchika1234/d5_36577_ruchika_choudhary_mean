function function1() {
    // array of numbers
    const numbers = [10, 20, 30, 40, 50]

    numbers.push(60)
    numbers.pop

    console.log(numbers)
    console.log(`type of numbers = ${typeof(numbers)}`)
}

// function1()

function function2() {
    // array of numbers
    // const numbers: number[] = [1, 2, 3, 4, 5]
    const numbers: Array<number> = [1, 2, 3, 4, 5]
    numbers.push(20)
    // numbers.push('20') // not allowed
    for (let index = 0; index < numbers.length; index++) {
        const number = numbers[index];
        console.log(`square of ${number} = ${number * number}`)
    }
}

// function2()

function function3() {
    //implicit
    // const countries = ['india', 'usa']

    // explicit
    // const country: string[] = ["india", "usa"]
    const country: Array<string> = ["india", "usa"]

}

function function4() {
    // const myArray: any[] = [10, 20, "test", true, {name: "ruchi"}]
    const myArray: Array<any> = [10, 20, "test", true, {name: "ruchi"}]
    
}