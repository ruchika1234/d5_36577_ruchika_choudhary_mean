// int num = 100;
let num: number
// console.log(`num = ${num}, type = ${typeof(num)}`)

// ok
num = 100
console.log(`num = ${num}, type = ${typeof(num)}`)

// not okk
// num = "str"

//string
const firstName: string = "ruchi"
console.log(`firstName = ${firstName}, type = ${typeof(firstName)}`)

//string
const lastName: string = 'ruchi'
console.log(`lastName = ${lastName}, type = ${typeof(lastName)}`)

//string
const address: string = `
address,
city,
state
`
console.log(`address = ${address}, type = ${typeof(address)}`)

//boolean
const canVote: boolean = true
console.log(`canVote = ${canVote}, type = ${typeof(canVote)}`)

//string
let myvar: undefined 
console.log(`myvar = ${myvar}, type = ${typeof(myvar)}`)

// myvar = 100

// object
const person: {name: string, age: number, address: string} = {name: "person1", age: 40, address: "pune"}
console.log(`person = ${person}, type = ${typeof(person)}`)

const mobile: object = {model: "iphone", price: 14400}
console.log(`mobile = ${mobile}, type = ${typeof(mobile)}`)

let result: number | string | boolean
result = 100
result = "ruchi" 
result = true

// all different types of values number, string, boolean, object, undefined
let myvar2: any
myvar2 = 100
myvar2 = "ruchi"
myvar2 = true
myvar2 = {namd: "test"}
myvar2 = undefined
