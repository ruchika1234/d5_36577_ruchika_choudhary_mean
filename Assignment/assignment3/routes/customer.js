const express = require('express')
const { request, response } = require('express')

const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/getData', (request, response) => {
  
    const statement = `select * from customer;`

    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })

})

router.post('/insertData', (request, response) => {
    const Name = request.body.Name
    const Password = request.body.Password
    const Mobile = request.body.Mobile
    const Address = request.body.Address
    const Email = request.body.Email

    const statement = `insert into customer (Name, Password, Mobile, Address, Email) values (
        '${Name}', '${Password}', '${Mobile}', '${Address}', '${Email}')`

    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
    
})

router.put('/updateData', (request, response) => {
    const ID = request.body.ID
    const Name = request.body.Name
    const Password = request.body.Password
    const Mobile = request.body.Mobile
    const Address = request.body.Address
    const Email = request.body.Email

    const statement = `update customer set
                       Name = '${Name}',
                       Password = '${Password}',
                       Mobile = '${Mobile}',
                       Address = '${Address}',
                       Email = '${Email}'
                       where ID = '${ID}'`

    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})

router.delete('/deleteData', (request, response) => {
    const ID = request.body.ID
    
    const statement = `delete from customer where ID = '${ID}'`

    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})

module.exports = router