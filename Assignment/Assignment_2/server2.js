// require the express package
const express = require('express')
const { response, request } = require('express')

const bodyParser = require('body-parser')

// create a server instance
const app = express()

app.use(bodyParser.json())

//require mysql for db connection
const mysql = require('mysql')

//---------------------
//----------product routes
//------------------------

app.get('/pizza', (request, response) => {

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `select * from PIZZA_ITEMS`

    //execute the statement
    connection.query(statement, (error, data) => {

        // close the connection
        connection.end()

        if (error) {
            response.end(`error: ${error}`)
        } else {

            console.log(data)

            // const string = JSON.stringify(data)

            response.send(data)

            // response.setHeader('Content-Type', 'application/json')

            // // data is array of products
            // // response.end(data)
            // response.end(string)
        }
    })
})

app.post('/pizza', (request, response) => {
    console.log(`body: `)
    console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `insert into PIZZA_ITEMS (Name, Type, Category, Description)
               values ('${request.body.Name}',
                       '${request.body.Type}',
                       '${request.body.Category}',
                       '${request.body.Description}')`


    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

app.put('/pizza', (request, response) => {
    console.log(`body: `)
    console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `update PIZZA_ITEMS set
                       Name = '${request.body.Name}',
                       Type = '${request.body.Type}',
                       Category = '${request.body.Category}',
                       Description = '${request.body.Description}'
                       where ID = '${request.body.ID}'`                   

    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

app.delete('/pizza', (request, response) => {
    console.log(`body: `)
    // console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `delete from PIZZA_ITEMS where ID = ${request.body.ID}`
    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

// listen on port 3000 (start the server)
app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
})
