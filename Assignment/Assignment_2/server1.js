// require the express package
const express = require('express')
const { response, request } = require('express')

const bodyParser = require('body-parser')

// create a server instance
const app = express()

app.use(bodyParser.json())

//require mysql for db connection
const mysql = require('mysql')

//---------------------
//----------product routes
//------------------------

app.get('/customer', (request, response) => {

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `select * from customer`

    //execute the statement
    connection.query(statement, (error, data) => {

        // close the connection
        connection.end()

        if (error) {
            response.end(`error: ${error}`)
        } else {

            console.log(data)

            // const string = JSON.stringify(data)

            response.send(data)

            // response.setHeader('Content-Type', 'application/json')

            // // data is array of products
            // // response.end(data)
            // response.end(string)
        }
    })
})

app.post('/customer', (request, response) => {
    console.log(`body: `)
    console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `insert into customer (Name, Password, Mobile, Address, Email)
               values ('${request.body.Name}',
                       '${request.body.Password}',
                       '${request.body.Mobile}',
                       '${request.body.Address}',
                       '${request.body.Email}')`


    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

app.put('/customer', (request, response) => {
    console.log(`body: `)
    console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `update customer set
                       Name = '${request.body.Name}',
                       Password = '${request.body.Password}',
                       Mobile = '${request.body.Mobile}',
                       Address = '${request.body.Address}'
                       where ID = '${request.body.ID}'`                   

    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

app.delete('/customer', (request, response) => {
    console.log(`body: `)
    // console.log(request.body)

    //create mysql connection
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
    })

    //open the connection
    connection.connect()

    //prepare the statement
    const statement = `delete from customer where ID = ${request.body.ID}`
    //execute the statement
    connection.query(statement, (error, data) => {

        connection.end()

        response.send(data)
    })

})

// listen on port 3000 (start the server)
app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
})

// const express = require('express')

// // const bodyParser = require('body-parser')

// const app = require('express')
// const { request, response } = require('express')

// // app.use(bodyParser.json())

// const mysql = require('mysql')

// app.get('/customer', (request, response) => {

//     const connection = mysql.createConnection({
//         host: 'localhost',
//         user: 'root',
//         password: 'password',
//         database: 'mydb'
//     })

//     connection.connect()

//     const statement = `select * from customer`

//     connection.query(statement, (error, data) => {

//         connection.end()

//         if(error){
//              response.end(`error: ${error}`)
//         } else {
//             console.log(data)

//             response.send(data)
//         }
//     })
// })

// app.listen(4000, '0.0.0.0', () => {
//     console.log("server started on port 4000")
// })