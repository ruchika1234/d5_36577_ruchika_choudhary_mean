const mysql = require('mysql')

function getProductsFromCustomer(){

    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb',
        port: 3306
    })

    const statement = `select * from customer`;

    connection.query(statement, (error, data) => {
        console.log(`error: ${error}`)

        console.table(data)

        connection.end()
    })
}

// getProductsFromCustomer()


function insertProductsInCustomer(){

    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb',
        port: 3306
    })

    const statement = `insert into customer(Name, Password, Mobile, Address, Email) values('Payal','343434','i-Phone','Banglore', 'payal23@gmail.com')`;

    connection.query(statement, (error, data) => {
        console.log(`error: ${error}`)

        console.log('data inserted successful')

        connection.end()
    })
}

// insertProductsInCustomer()

function getProductsFromPizza(){

    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb',
        port: 3306
    })

    const statement = `select * from PIZZA_ITEMS`;

    connection.query(statement, (error, data) => {
        console.log(`error: ${error}`)

        console.table(data)

        connection.end()
    })
}

getProductsFromPizza()

function insertProductsInPizza(){

    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb',
        port: 3306
    })

    const statement = `insert into PIZZA_ITEMS(Name, Type, Category, Description) values('Amit', 'panner makhhan', '4 piece', 'very nice')`;

    connection.query(statement, (error, data) => {
        console.log(`error: ${error}`)

        console.log('data inserted successful')

        connection.end()
    })
}

// insertProductsInPizza()