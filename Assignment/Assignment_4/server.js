const express = require('express')
const { request } = require('express')
const bodyParser = require('body-parser')
const config = require('./config')
const jwt = require('jsonwebtoken')
const morgan = require('morgan')

const customerRoute = require('./routes/customers')
const bookRoute = require('./routes/bookShop')

const app = express()

// function getUserId(request, response, next){
//     if(request.url == '/customer/signin' || request.url == '/customer/signup'){
//         next()
//     } else {
//          try{
//              const token = request.headers['token']
//              const data = jwt.verify(token, config.secret)

//              request.customerId = data['id']

//              next()
//          } catch(ex) {

//             response.status(401)
//             response.send({status: 'error', error: 'protected api'})

//          }
//     }
// }

// app.use(getUserId)

app.use(bodyParser.json())
app.use(morgan('combined'))

app.use('/customer', customerRoute)
app.use('/book-shop', bookRoute)

app.get('/', (request, response) => {
    response.send('GET/ method')
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})