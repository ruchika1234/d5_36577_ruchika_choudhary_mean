const express = require('express')
const db = require('../db')
const utils = require('../utils')
const config = require('../config')
const jwt = require('jsonwebtoken')
const crypto = require('crypto-js')
const { request, response } = require('express')
const { createError, createSuccess } = require('../utils')

const router = express.Router()

//------------------------------------------
// GET
//-----------------------------------------

router.get('/distinct', (request, response) => {

    const statement = `select distinct subject from books`

    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send({ status: 'error', error: 'user not found' })
            } else {
                response.send(createSuccess(data))
            }
        }
    })
})

router.get('/bySubject/:subject', (request, response) => {
    const { subject } = request.params

    const statement = `select name, author, subject, price from books where subject = '${subject}'`

    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send({ status: 'error', error: 'user not found' })
            } else {
                response.send(createSuccess(data))
            }
        }
    })
})

router.get('/all-books', (request, response) => {
    const statement = `select * from books`
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send({ status: 'error', error: 'user not found' })
            } else {
                response.send(createSuccess(data))
            }
        }
    })
})

router.get('/getById/:id', (request, response) => {
    const { id } = request.params

    const statement = `select * from books where id = '${id}'`
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send({ status: 'error', error: 'book not found' })
            } else {
                response.send(createSuccess(data))
            }
        }
    })
})

//------------------------------------------

//------------------------------------------
// POST
//-----------------------------------------

router.post('/add-book', (request, response) => {
    const { id, name, author, subject, price } = request.body

    // const encryptedPassword = crypto.SHA256(password)
    const statement = `insert into books(id, name, author, subject, price ) values(
     '${id}', '${name}', '${author}', '${subject}', '${price}'
    )`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})



//------------------------------------------

//------------------------------------------
// PUT
//-----------------------------------------

router.put('/update/:id', (request, response) => {
    const { id } = request.params
    const { name, author, subject, price } = request.body

    const statement = `update books set 
                  name = '${name}',
                  author = '${author}',
                  subject = '${subject}',
                  price = '${price}'
                  where id = '${id}'`

    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send({ status: 'error', error: 'book not found' })
            } else {
                response.send(createSuccess(data))
            }
        }
    })

})

//------------------------------------------

//------------------------------------------
// DELETE
//-----------------------------------------

router.delete('/delete/:id', (request, response) => {
    const { id } = request.params

    const statement = `delete from books where id = '${id}'`

    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send({ status: 'error', error: 'user not found' })
            } else {
                response.send(createSuccess(data))
            }
        }
    })
})

//------------------------------------------
module.exports = router