const express = require('express')
const db = require('../db')
const utils = require('../utils')
const config = require('../config')
const jwt = require('jsonwebtoken')
const crypto = require('crypto-js')
const { request, response } = require('express')
const { createError, createSuccess } = require('../utils')

const router = express.Router()

//------------------------------------------
// GET
//-----------------------------------------

router.get('/get', (request, response) => {
    const statement = `select * from customers`
    db.query(statement, (error, data) => {
        if(error){
            response.send(utils.createError(error))
        } else {
            if(data.length == 0){
                response.send({status: 'error', error: 'user not found'})
            } else{
                response.send(createSuccess(data))
            }
        }
    })
})

router.get('/getById/:id', (request, response) => {
    const { id } = request.params

    const statement = `select * from customers where id = '${id}'`
    db.query(statement, (error, data) => {
        if(error){
            response.send(utils.createError(error))
        } else {
            if(data.length == 0){
                response.send({status: 'error', error: 'user not found'})
            } else{
                response.send(createSuccess(data))
            }
        }
    })
})

//------------------------------------------

//------------------------------------------
// POST
//-----------------------------------------

router.post('/signup', (request, response) => {
    const {name, mobile, address, email, password, birth} = request.body

    // const encryptedPassword = crypto.SHA256(password)
    const statement = `insert into customers(name, password, mobile, address, email, birth) values(
        '${name}', '${password}', '${mobile}', '${address}', '${email}', '${birth}'
    )`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

router.post('/signin', (request, response) => {
    const { email, password } = request.body

    // const encryptedPassword = crypto.SHA256(password)
    const statement = `select id, name, mobile, address, email, birth from customers where email = '${email}' and password = '${password}'`


    db.query(statement, (error, customers) => {
        if(error){
            response.send(utils.createError(error))
        } else {
            if(customers.length == 0){
                response.send(utils.createError(error))
            } else {
                const customer = customers[0]
                // const token = jwt.sign({id: customer['id']}. config.secret)
                response.send(utils.createSuccess( {
                    name: customer['name'],
                    mobile: customer['mobile'],
                    address: customer['address'],
                    email: customer['email'],
                    birth: customer['birth'],
                    

                }))
            }
        }
    })
})


//------------------------------------------

//------------------------------------------
// PUT
//-----------------------------------------

router.put('/update/:id', (request, response) => {
    const { id } = request.params
    const {name, password, mobile, address, email, birth} = request.body

    const statement = `update customers set 
                  name = '${name}',
                  password = '${password}',
                  mobile = '${mobile}',
                  address = '${address}',
                  email = '${email}',
                  birth  = '${birth}'
                  where id = '${id}'`

                  db.query(statement, (error, data) => {
                    if(error){
                        response.send(utils.createError(error))
                    } else {
                        if(data.length == 0){
                            response.send({status: 'error', error: 'user not found'})
                        } else{
                            response.send(createSuccess(data))
                        }
                    }
                })            
    
})

//------------------------------------------

//------------------------------------------
// DELETE
//-----------------------------------------

router.delete('/delete/:id', (request, response) => {
    const { id } = request.params

    const statement = `delete from customers where id = '${id}'`
    
    db.query(statement, (error, data) => {
        if(error){
            response.send(utils.createError(error))
        } else {
            if(data.length == 0){
                response.send({status: 'error', error: 'user not found'})
            } else{
                response.send(createSuccess(data))
            }
        }
    })  
})

//------------------------------------------
module.exports = router