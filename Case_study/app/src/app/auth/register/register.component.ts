import { RegisterService } from './../register.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  firstname = ''
  lastname = ''
  address = ''
  contact = 0
  email = ''
  password = ''

  admin = []

  constructor(
    private registerService: RegisterService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // const id = this.activateRoute.snapshot.queryParams['id']
    //   if(id){
    //  this.registerService.getAdmin(id)
    //  .subscribe(response => {
    //    if(response['status'] == 'success'){
    //      const admins = response['data']
    //      if(admins.length > 0){
    //       this.admin = admins[0]
    //       this.firstname = this.admin['FirstName']
    //       this.lastname = this.admin['LastName']
    //       this.address = this.admin['Address']
    //       this.contact = this.admin['Contact']
    //       this.email = this.admin['Email']
    //       this.password = this.admin['Password']

          
    //      }
    //    }
    //  })
    // }
  }

  onRegister(){
    this.registerService.register(this.firstname, this.lastname, this.address, this.contact, this.email, this.password)
    .subscribe(response => {
      if(response['status'] == 'success'){
        console.log(response['data']);
        alert("register successfully")
        
      } else{
        alert("error")
        console.log(response['errro']);
        
      }
    })
  }

}
