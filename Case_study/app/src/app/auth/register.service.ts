import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  url = 'http://localhost:3000/admin'

  constructor(
    private router :Router,
    private httpClient: HttpClient
  ) { }

  register(firstname: string, lastname: string, address: string, contact: number, email: string, password: string){
    const body = {
      firstname: firstname, 
      lastname: lastname,
      address: address,
      contact: contact,
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/register', body)
  }

  getAdmin(id){
    return this.httpClient.get(this.url + '/details' + id)
  }

  editProfile(id, firstname: string, lastname: string, address: string, contact: number, email: string, password: string){
    const body = {
      firstname: firstname,
      lastname: lastname,
      address: address,
      contact: contact,
      email: email,
      password: password
    }

    return this.httpClient.put(this.url + '/edit-profile' + id, body)

  }
}
