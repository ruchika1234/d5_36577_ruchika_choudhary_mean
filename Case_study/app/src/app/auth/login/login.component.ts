import { LoginService } from './../login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = ''
  password = ''

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onLogin(){
    this.loginService.login(this.email, this.password)
    .subscribe(response => {
      if(response['status'] == 'success'){
        const data = response['data']
        console.log(data);


        this.router.navigate(['/home'])
        
      } else{
        alert("invalid email or password")
        console.log(response['error']);
        
      }
    })
  }
}
