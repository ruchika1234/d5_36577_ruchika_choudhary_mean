const express = require('express')
const bodyParser = require('body-parser')
const { request, response } = require('express')
const jwt = require('jsonwebtoken')
const cors = require('cors')

//morgan: for logging:
const morgan = require('morgan')


//routers
const adminRouter = require('./admin-routes/admin')

const app = express()
app.use(cors('*'))
app.use(bodyParser.json())
app.use(morgan('combined'))

// // add a middleware for getting the id from token
// function getUserId(request, response, next) {

//     if(request.url == '/admin/signup'  || request.url == '/admin/signin'){
//         next()
//     } else {

//     try {
//         const token = request.headers['token']
//         const data = jwt.verify(token, config.secret)

//         // add a new key named userId with logged in user's id
//         request.userId = data['id']

//         //gomto the actual route
//         next()


//     } catch(ex){
//         response.status(401)
//         response.send({status: 'error', error: 'protected api'})

//     }

// }
    
// }

// app.use(getUserId)

//requires to send static images in the directory named image    
app.use(express.static('images/'))


// add the routes
app.use('/admin', adminRouter)



// default route
app.get('/', (request, response) => {
    response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})