const express = require('express')
const utils = require('../utils.js')
const db = require('../db')
// const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')

const router = express.Router()
//-----------------------------------------------------------
// GET
//-----------------------------------------------------------

router.get('/profile', (request, response) => {
    const statement = `select FirstName, LastName, Address, Contact, Email, Password from Admin`
    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'admin not found' })
            } else {
                const admin = admins[0]
                response.send(utils.createResult(error, users))
            }
        }
    })

})

router.get('/details/:id', (request, response) => {
    const {id} = request.params
    const statement = `select FirstName, LastName, Address, Contact, Email, Password from Admin where id = ${id}`
    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'admin not found' })
            } else {
                const admin = admins[0]
                response.send(utils.createResult(error, users))
            }
        }
    })

})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST
//-----------------------------------------------------------

router.post('/register', (request, response) => {
    const { firstName, lastName, address, contact, email, password } = request.body

    // const encryptedPassword = crypto.SHA256(password)
    const statment = `insert into Admin (FirstName, LastName, Address, Contact,  Email, Password) values (
      '${firstName}', '${lastName}', '${address}', '${contact}', '${email}', '${password}'
  )`

    db.query(statment, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

router.post('/login', (request, response) => {
    const { email, password } = request.body

    // const encryptedPassword = crypto.SHA256(password)
    const statement = `select id, FirstName, LastName from Admin where Email = '${email}' and Password = '${password}'`

    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'admin not found' })
            } else {
                const admin = admins[0]
                // const token = jwt.sign({ id: admin['id'] }, config.secret)
                response.send(utils.createResult(error, {
                    FirstName: admin['FirstName'],
                    LastName: admin['LastName'],

                }))
            }
        }
    })
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT
//-----------------------------------------------------------


router.put('/edit-profile', (request, response) => {
    const {id} = request.params
    const {firstname, lastname, address, contact, email, password} = request.body

    const statement = `update Admin set 
               FirstName = ${firstName},
               LastName = ${lastname},
               Address = ${address},
               Contact = ${contact},
               Email = ${email},
               Password = ${password}
               where Id = ${id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })           
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE
//-----------------------------------------------------------


router.delete('', (request, response) => {
    response.send()
})

//-----------------------------------------------------------


module.exports = router